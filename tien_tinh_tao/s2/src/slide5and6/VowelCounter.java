
package slide5and6;


public class VowelCounter {
    public static int Counter(String string,char c){
        int count = 0;
        for(char aChar : string.toLowerCase().toCharArray()){
            if(aChar == c){
                count ++;
            }
        }
        return count;
    }
    public static void vowelCounter(String string){
        System.out.println("a "+Integer.toString(Counter(string, 'a')));
        System.out.println("e "+Integer.toString(Counter(string, 'e')));
        System.out.println("i "+Integer.toString(Counter(string, 'i')));
        System.out.println("o "+Integer.toString(Counter(string, 'o')));
        System.out.println("u "+Integer.toString(Counter(string, 'u')));
    }
    public static void main(String[] args){
        String string = "The quick brown fox jumps over the lazy dog";
        VowelCounter.vowelCounter(string);
    }
}
