package slide5and6;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author vietn
 */
public class Hangman {

    private static final String string = "TooManyExcercise!";
    private boolean[] solved;

    public Hangman() {
        solved = new boolean[Hangman.string.length()];
        for (int i = 0; i < Hangman.string.length(); i++) {
            solved[i] = false;
        }
    }

    public boolean solved() {
        for (int i = 0; i < this.solved.length; i++) {
            if (this.solved[i] == false) {
                return false;
            }
        }
        return true;
    }

    public void render() throws IOException {
        System.out.println("Press a character\n");
        for (int i = 0; i < this.solved.length; i++) {
            if (solved[i] == false) {
                System.out.print("_");

            } else {
                System.out.print(Hangman.string.charAt(i));
            }
        }
        System.out.println("\n");
    }

    public void check(char c) {
        String tmpstring = Hangman.string.toLowerCase();
        for (int i = 0; i < tmpstring.length(); i++) {
            if (this.solved[i] == false) {
                if (c == tmpstring.charAt(i)) {
                    this.solved[i] = true;
                    return;
                }
            }
        }
    }

    public void gamePlay() throws IOException {

        while (!this.solved()) {
            this.render();
            Scanner scanner = new Scanner(System.in);
            char c = scanner.next().toLowerCase().toCharArray()[0];
            this.check(c);
        }

    }

    public static void main(String[] args) throws IOException {
        Hangman hangman = new Hangman();
        hangman.gamePlay();
        System.out.println("\n\n\n");
        System.out.println("Done");
        System.out.println("The string we have   " + Hangman.string);

    }
}
