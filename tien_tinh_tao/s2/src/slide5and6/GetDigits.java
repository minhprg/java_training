/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slide5and6;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.util.*;

/**
 *
 * @author vietn
 */
public class GetDigits {

    private ArrayList<Integer> digits;

    /**
     *
     * @param number
     */
    public GetDigits(Integer number) {
        digits = new ArrayList<Integer>();
        getDigits(number);
    }

    /**
     *
     * @param number
     * @return
     */
    public static Integer getLastDigit(Integer number) {
        return number % 10;
    }

    public void getDigits(Integer number) {
        if (number > 0) {
            digits.add(GetDigits.getLastDigit(number));
            number = number / 10;
            getDigits(number);
        }
        return;

    }

    public String toString() {
        String string = "";
        for (Integer integer : digits) {
            string += " " + integer.toString();
        }
        return string;
    }

    public static void main(String[] args) {
        GetDigits test = new GetDigits(3255);
        System.out.println(test.toString());
//        test.toString();
    }
}
