/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slide5and6;

import java.util.Scanner;

/**
 *
 * @author vietn
 */
enum Days {
    Sunday(1, "Sunday") ,
    Monday(2, "Monday"),
    Tuesday(3, "Tuesday"),
    Wednesday(4, "Wednesday"),
    Thursday(5, "Thursday"),
    Friday(6, "Friday"),
    Saturday(7, "Saturday");
    private int key;
    private String name;

    Days(int key, String name) {
        this.key = key;
        this.name = name;
    }

    Days(int key) {
//        String name;
        this.key = key;
        for(Days day : Days.values()){
            if(day.key == key){
                this.name = day.name;
//                name = day.name;
            }
        }
    }

       
    
    public String toString() {
        return this.name;
    }
}

public class WhichDay {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i=1;
        do{
            i = scanner.nextInt();
        }while(!(i>0 && i<8));
        Days aDay = Days.values()[i-1];
        System.out.println(aDay.toString());
                
    }

}
