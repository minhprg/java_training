package slide9.bai1;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author vietn
 */
public class bai1 {
    public static void main(String[] args){
        System.out.println("Nhap bang cua so dong lenh");
        List<String> listString = Arrays.asList(args);
        String content = String.join("",listString);
        String terminalContent  = "";
        for(Character c : content.toCharArray()){
            if(!terminalContent.contains(c.toString())){
                terminalContent+=c;
            }
        }
        System.out.println(terminalContent);
    }
}
