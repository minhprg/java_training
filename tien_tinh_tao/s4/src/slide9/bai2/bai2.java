package slide9.bai2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author vietn
 */
public class bai2 {

    private Integer[] integer;
    private static Random random;
    private static int numberOfElement;
    private static int maxValueOfElement;
    public Integer[] getInteger() {
        return integer;
    }

    public void setInteger(Integer[] integer) {
        this.integer = integer;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public bai2() {
        random = new Random();
        numberOfElement = this.random.nextInt(30);
        maxValueOfElement = this.random.nextInt(30);
    }
    
    public static Integer[] spanArrayOfInteger() {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < numberOfElement; i++) {
            list.add(random.nextInt(maxValueOfElement));
        }
        return list.toArray(new Integer[0]);
    }
    public static Integer[] sort(Integer[] integer){
        Arrays.sort(integer);
        return bai2.reverse(integer);
    }
    public static Integer[] reverse(Integer[] integer){
        List<Integer> tempInteger = Arrays.asList(integer);
        Collections.reverse(tempInteger);
        return tempInteger.toArray(integer);
    }
    public static void main(String[] args) {
        bai2 newbai2 = new bai2();
        newbai2.setInteger(bai2.spanArrayOfInteger());
        newbai2.setInteger(bai2.sort(newbai2.getInteger()));
        System.out.println(Arrays.toString(newbai2.integer));
    }
}
