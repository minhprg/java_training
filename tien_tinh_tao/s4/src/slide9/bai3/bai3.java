package slide9.bai3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import slide9.bai2.*;
/**
 *
 * @author vietn
 */
public class bai3 extends bai2{
    
    private static int numberOfFilter = 3;
    public bai3() {
        super();
        this.setInteger(bai2.spanArrayOfInteger());
    }

    public static int getNumberOfFilter() {
        return numberOfFilter;
    }

    public static void setNumberOfFilter(int numberOfFilter) {
        bai3.numberOfFilter = numberOfFilter;
    }
    public static Integer[] filter(bai3 abai3){
        Integer[] tempInteger =  bai3.sort(abai3.getInteger());
        tempInteger = Arrays.copyOfRange(tempInteger, 0, numberOfFilter);
        return bai3.reverse(tempInteger);
    }
    public static void main(String[] args){
        bai3 newbai3 = new bai3();
        System.out.println(Arrays.toString(newbai3.getInteger()));
        System.out.println(Arrays.toString(bai3.filter(newbai3)));
    }
}
