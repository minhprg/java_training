/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Task3;

import java.util.Scanner;

/**
 *
 * @author Administrator
 */
public class ReverseText {
    public static void main(String[] args) {
        String s;
        Scanner sc = new Scanner(System.in);
        System.out.println("sc = ");
        s = sc.nextLine();
        String reverse = new StringBuffer(s).reverse().toString();
        
        System.out.println("string before = "+s);
        System.out.println("string after = "+reverse);
        
    }
    
}
