package Bai4;

import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai4 {
    public static void findVowel(String s){
        int a=0,e=0,i=0,o=0,u=0;
        for(int j=0;i<s.length()-1;j++){
            switch (s.charAt(i)) {
                case 'a':
                case 'A':
                    a++;
                    break;
                case 'e':
                case 'E':
                    e++;
                    break;
                case 'i':
                case 'I':
                    i++;
                    break;
                case 'o':
                case 'O':
                    o++;
                    break;
                case 'u':
                case 'U':
                    u++;
                    break;
                default:
                    break;
            }
        }
        if(a!=0){
            System.out.println("a:"+a);
        }
        if(e!=0){
            System.out.println("e:"+e);
        }
        if(i!=0){
            System.out.println("i:"+i);
        }
        if(o!=0){
            System.out.println("o:"+o);
        }
        if(u!=0){
            System.out.println("u:"+u);
        }
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s=sc.next();
        findVowel(s);
    }
}
