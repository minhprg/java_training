package Bai2;

import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class DayInWeek {
    public enum Day{
        Monsday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday;
    }
    
    public static void testDay(int a){
        switch(a){
            case 1:
                System.out.println(Day.Monsday);
                break;
            case 2:
                System.out.println(Day.Tuesday);
                break;
            case 3:
                System.out.println(Day.Wednesday);
                break;
            case 4:
                System.out.println(Day.Thursday);
                break;
            case 5:
                System.out.println(Day.Friday);
                break;
            case 6:
                System.out.println(Day.Saturday);
                break;
            case 7:
                System.out.println(Day.Sunday);
                break;    
        }         
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int num=sc.nextInt();
        testDay(num);
    }
}
