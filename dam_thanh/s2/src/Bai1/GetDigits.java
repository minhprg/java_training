package Bai1;

import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class GetDigits {
    public static void getDigits(int number){
        int temp;
        int[] array=new int[100];
        int i=0;
        while(number!=0){
            temp=number%10;
            number=number/10;
            array[i]=new Integer(temp);
            i++;
        }
        for(int j=0;j<i;++j){
            System.out.print(array[j]+" ");
        }
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int a;
        a=sc.nextInt();
        getDigits(a);
    }
}
