package Bai4;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai4 {

    public static void mergeTwoArray(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        ArrayList<Integer> tmp1 = new ArrayList<>();
        ArrayList<Integer> tmp2 = new ArrayList<>();
        for (int i = 0; i < list1.size() / 2; i++) {
            tmp1.add(list1.get(i));
            tmp2.add(list2.get(i));
        }
        for (int i = 0; i < list1.size() / 2; i++) {
            list1.set(i, tmp2.get(i));
            list2.set(i, tmp1.get(i));
        }
    }

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhập độ dài của 2 mảng: ");
            int n = sc.nextInt();
            ArrayList<Integer> list1 = new ArrayList<>();
            ArrayList<Integer> list2 = new ArrayList<>();
            System.out.println("Lần lượt nhập các phần tử của 2 mảng: ");
            for (int i = 0; i < n; i++) {
                list1.add(sc.nextInt());
                list2.add(sc.nextInt());
            }
            mergeTwoArray(list1, list2);
            System.out.println(list1);
            System.out.println(list2);
        } catch (Exception e) {
        }
    }
}
