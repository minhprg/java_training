package Bai3;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai3 {

    public static void reverseArray(ArrayList<Integer> listnumber) {
        Stack st = new Stack();
        for (int i = 0; i < listnumber.size(); i++) {
            st.push(listnumber.get(i));
        }
        for (int i = 0; i < listnumber.size(); i++) {
            listnumber.set(i, (Integer) st.pop());
        }
    }

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhập độ dài của mảng: ");
            int n = sc.nextInt();
            ArrayList<Integer> listnumber = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                listnumber.add(sc.nextInt());
            }
            reverseArray(listnumber);
            for (int i = 0; i < n; i++) {
                System.out.print(listnumber.get(i) + " ");
            }
        } catch (Exception e) {
            System.out.println("Lỗi kí tự");
        }
    }
}
