package Bai2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai2 {

    public static void sort1(ArrayList<Integer> number) {
        for (int i = 0; i < number.size() - 1; i++) {
            for (int j = i + 1; j < number.size(); j++) {
                if (number.get(j) < number.get(i)) {
                    int tmp = number.get(j);
                    number.set(j, number.get(i));
                    number.set(i, tmp);
                }
            }
        }
    }

    public static void sort2(ArrayList<Integer> number) {
        for (int i = 0; i < number.size() - 1; i++) {
            for (int j = number.size() - 1; j >= i + 1; j--) {
                if (number.get(j) < number.get(i)) {
                    int tmp = number.get(j);
                    number.set(j, number.get(i));
                    number.set(i, tmp);
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Nhap do lon cua mang: ");
            int n = sc.nextInt();
            ArrayList<Integer> listnumber = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                listnumber.add(sc.nextInt());
            }
            //sort1(listnumber);
            sort2(listnumber);
            for (int i = 0; i < n; i++) {
                System.out.print(listnumber.get(i) + " ");
            }
        } catch (Exception e) {
            System.out.println("Lỗi kí tự");
        }

    }
}
