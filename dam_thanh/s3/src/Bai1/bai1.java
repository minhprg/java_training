package Bai1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai1 {

    public static void shuffle1(ArrayList list) {
        Collections.shuffle(list);
    }

    public static ArrayList shuffle2(ArrayList list) {
        Vector v=new Vector();
        int tmp=list.size();
        ArrayList listresult=new ArrayList();
        for(int i=0;i<tmp;i++){
            listresult.add(i);
        }
        for(int i=0;i<list.size();){
            int rd= (int)(Math.random()*tmp);
            if(!v.contains(rd)){
                listresult.set(i, list.get(rd));
                v.add(rd);
                i++;
            }
        }
        return listresult;
    }
    
    public static void main(String[] args) {
        ArrayList listtest=new ArrayList();
        for(int i=0;i<10;i++){
            listtest.add(i);
        }
        //shuffle1(listtest);
        ArrayList result=new ArrayList();
        result=shuffle2(listtest);
        System.out.println(result);
    }
}    
