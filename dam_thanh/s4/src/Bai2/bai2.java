package Bai2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai2 {

    public static void sortArray(List<Integer> number) {
        Collections.sort(number);
        Collections.reverse(number);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập độ dài của mảng: ");
        int size=sc.nextInt();
        System.out.println("Nhập mảng các số: ");
        List<Integer> number1 = new ArrayList<>();
        for(int i=0;i<size;i++){
            number1.add(sc.nextInt());
        }
        sortArray(number1);
        for (int i = 0; i < number1.size(); i++) {
            System.out.print(number1.get(i) + " ");
        }
    }
}
