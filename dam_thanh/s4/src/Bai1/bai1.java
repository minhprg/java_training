package Bai1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Dam Tien Thanh
 */
public class bai1 {
    public static List rewriteString(String s){
        List<String> result=new ArrayList<>();
        for(int i=0;i<s.length();i++){
            if(!result.contains(s.charAt(i)+"")){
                result.add(s.charAt(i)+"");
            }
        }
        return result;
    }
    
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Nhập chuỗi: ");
        String s1=sc.nextLine();
        List s2= rewriteString(s1);
        for(int i=0;i<s2.size();i++){
            System.out.print(s2.get(i));
        }
        System.out.println();
    }
}
