import java.util.Scanner;
import java.util.regex.Pattern;

public class Ex1_XoaKiTuLap {
	public static void XuLyChuoi(String a0) {
		String pattern = "(.)\\1+";		
		System.out.print("text thay the: ");
		System.out.println(a0.replaceAll(pattern, "$1"));
	}
	
	public static void main(String[] args) {
		//test chuoi cho truoc
		String a = "aaaabbdkfcc";
		System.out.println("text da nhap:"+ a);
		Ex1_XoaKiTuLap.XuLyChuoi(a);
		//test chuoi nhap vao
		System.out.println("nhap text moi:");
		Scanner chuoi = new Scanner(System.in);
		String a2 = chuoi.nextLine();
		Ex1_XoaKiTuLap.XuLyChuoi(a2);
		
	}

}
