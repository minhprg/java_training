import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

public class Ex2_OrderedArray {

	public static void main(String[] args) {
	ArrayList<Integer> array = new ArrayList<>();
	//phuong phap 1, su dung Collection.sort va reverseOrder
	int i = 0;
	while (i<10) {
		int n = new Random().nextInt(100);
		array.add(n);
		i++;
	}
	System.out.println("mang nhap vao: "+ array);	
	Collections.sort(array, Collections.reverseOrder());
	
	System.out.println("mang sap xep tu lon toi nho: ");
	for (int j:array) {
		System.out.print(j+",");
	}

	
	}
}
