import java.util.Scanner;

public class Ex3_ReverseText
{

	public static void main(String[] args) {
		String string1;
		
		System.out.println("enter your string:");
		string1=new Scanner(System.in).nextLine();
		//convert chuoi1 thanh mang ki tu
		char[] mang1=string1.toCharArray();
		//lap mang ki tu 2 de truyen chuoi dao nguoc vao
		char[] mang2 = new char [ mang1.length];
		System.out.println("your new string: ");
		int j=0;
		for (int i=mang1.length-1;i>=0;i--)
		{
			mang2[j]=mang1[i];
			j++;
		}
		//chuyen toan bo mang ki tu 2 thanh chuoi 2
		String chuoi2 = String.copyValueOf(mang2);
		
		//in chuoi moi ra
		System.out.print(chuoi2);

	}

}
