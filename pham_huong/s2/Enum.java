/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai2;

import java.util.Scanner;

/**
 *
 * @author huong
 */
public class Enum {
     public enum DaysOfWeek{ MONDAY, TUESDAY, WEDNESDAY,
      THURSDAY, FRIDAY, SATURDAY, SUNDAY
     }
    public static void main(String[] args) {
        
        Scanner sc= new Scanner(System.in);
        int i=sc.nextInt();
        switch(i){
            case 1:
                System.out.print(DaysOfWeek.MONDAY);
                break;
            case 2:
                System.out.print(DaysOfWeek.TUESDAY);
                break;
            case 3:
                System.out.print(DaysOfWeek.WEDNESDAY);
                break;
            case 4:
                System.out.print(DaysOfWeek.THURSDAY);
                break;
            case 5:
                System.out.print(DaysOfWeek.FRIDAY);
                break;
            case 6:
                System.out.print(DaysOfWeek.SATURDAY);
                break;
            default:
                 System.out.print(DaysOfWeek.SUNDAY);
                 break;
        }
    }
    
}
