
package Week51OfYear;

import java.util.Scanner;

/**
 *
 * @author Hoang Phu
 */
public class Bai1 {
    private String str;
    
    public void getInput(){
        Scanner var = new Scanner(System.in);
        str = var.nextLine();
    }
    
    public String clearSameChar(){
        String result = "";
        for(int i = 0; i < str.length(); i++){
            if(str.indexOf(str.charAt(i)) == i){
                result += str.charAt(i);
            }
        }
        return result;
    }
    
    public static void main(String[] args) {
        Bai1 bai1 = new Bai1();
        System.out.println("Nhập vào một chuỗi kí tự: ");
        bai1.getInput();
        System.out.println("Chuỗi cắt bỏ các kí tự trùng nhau: " + bai1.clearSameChar());
    }
}
