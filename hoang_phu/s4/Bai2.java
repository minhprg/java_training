package Week51OfYear;

import java.util.*;

/**
 *
 * @author Hoang Phu
 */
public class Bai2 {

    protected List<Integer> array;

    public void setArray() {
        array = new ArrayList<>();
        Scanner var = new Scanner(System.in);
        System.out.print("Nhập số lượng phần tử: ");
        int size = var.nextInt();
        int element;
        System.out.println("Nhập vào " + size + " phần tử mảng:");
        for (int i = 0; i < size; i++) {
            element = var.nextInt();
            array.add(element);
        }
    }

    public static Comparator<Integer> number = new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            return o2 - o1;
        }
    };

    public void sortArray() {
        Collections.sort(array, number);
    }
    
    public void display(){
        for(int i = 0; i < array.size(); i++){
            System.out.println(array.get(i) + "   ");
        }
    }
    
    public static void main(String[] args) {
        Bai2 bai2 = new Bai2();
        bai2.setArray();
        bai2.sortArray();
        System.out.println("Mảng sắp xếp: ");
        bai2.display();
    }
}
