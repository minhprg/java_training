
package Week51OfYear;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Hoang Phu
 */
public class Bai3 extends Bai2{
    private int k;
    
    public void init(){
        setArray();
        Scanner var = new Scanner(System.in);
        System.out.print("Nhap k: ");
        k = var.nextInt();
    }
    
    public void showMaxElements(){
        sortArray();
        for(int i = 0; i < k; i++){
            System.out.println(array.get(i) + "  ");
        }
    }
    
    
    public static void main(String[] args) {
        Bai3 bai3 = new Bai3();
        bai3.init();
        System.out.println("Cac phan tu lon: ");
        bai3.showMaxElements();
    }
}
