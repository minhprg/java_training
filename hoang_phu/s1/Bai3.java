class Cat{
	private int name;
	private int age;
	
	public void displayName(){
		System.out.println(this.name);
	}
	
	public void displayAge(){
		System.out.println(this.age);
	}

	public void speak(){
		System.out.println("Meow");
	}
}