class Person{
	private String name;
	
	Person(String name){
		this.name = name;
	}
	
	public String toString(){
		return "My name is " + name;
	}
	
	public static void main(){
		Person onePerson = new Person("Hoàng Văn Phú");
		System.out.println(onePerson.toString());
	}
}