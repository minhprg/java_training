package Week3;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hoangphu
 */
public class Cau4 {

    List<Integer> array1;
    List<Integer> array2;

    /**
     * Thiet lap mang
     */
    public void init() {
        System.out.println("Nhap vao mang so nguyen thu nhat: ");
        array1 = getArray();
        System.out.println("");
        System.out.println("Nhap vao mang so nguyen thu hai: ");
        array2 = getArray();
        System.out.println("");
    }

    protected List<Integer> getArray() {
        List<Integer> array = new ArrayList<>();
        Scanner var = new Scanner(System.in);
        int x;
        int key = 1;
        while(!(key == 0)){
            System.out.print("Nhap phan tu: ");
            try{
                x = var.nextInt();
                array.add(x);
            }catch(InputMismatchException e){
                System.err.println("Chi nhap so!!");
            }
            System.out.print("Nhap 0 de ket thuc nhap: ");
            key = var.nextInt();
        }
        return array;
    }

    public void puzzle() {
        List<Integer> temp1 = new ArrayList<>();
        List<Integer> temp2 = new ArrayList<>();
        int size1 = array1.size();
        int size2 = array2.size();
        int index = 0;
        while(index < size1 || index < size2){
            if(index < size1 / 2){
                temp1.add(array1.get(index));
            }else if(index < size1){
                temp2.add(array1.get(index));
            }
            if(index < size2 / 2){
                temp2.add(array2.get(index));
            }else if(index < size2){
                temp1.add(array2.get(index));
            }
            index++;
        }
        array1 = temp1;
        array2 = temp2;
    }

    public void displayArray() {
        System.out.println("Mang 1: ");
        for (Integer x : array1) {
            System.out.print(x + "    ");
        }
        System.out.println("\nMang 2: ");
        for (Integer x : array2) {
            System.out.print(x + "    ");
        }
    }

    public static void main(String[] args) {
        Cau4 c4 = new Cau4();
        c4.init();
        c4.puzzle();
        c4.displayArray();
    }
}
