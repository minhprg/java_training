package Week3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author hoangphu
 */
public class Cau1 {

    Scanner var = new Scanner(System.in);
    List<Integer> array;

    /**
     * Thiet lap mang
     */
    public void init() {
        this.array = new ArrayList<>();
    }

    /**
     * Nhap cac phan tu mang
     */
    public void getArray() {
        int x = 0;
        Boolean key = true;
        init();
        System.out.println("Ket thuc nhap bang ki tu khac so");

        while (true) {
            System.out.print("Nhap vao mot so nguyen: ");
            key = var.hasNextInt();
            if (!key) {
                break;
            }
            try {
                x = var.nextInt();
            } catch (InputMismatchException e) {
                System.err.print("enter number, please!");
            }
            array.add(x);
        }

    }

    /**
     * Xao tron su dung lop Collections
     */
    public void shuffle() {
        Collections.shuffle(array);
    }

    /**
     * Xao tron mang tu thiet ke
     */
    public void shuffle2() {
        List<Integer> tempArray = new ArrayList<>();
        int index;
        int size = array.size();
        for (int i = 0; i < size; i++) {
            index = (int) (Math.random() * array.size());
            tempArray.add(array.get(index));
            array.remove(index);
        }
        array = tempArray;

    }

    /**
     * Sap xep mang su dung lop Collections
     */
    public void sort() {
        Collections.sort(array);
    }

    /**
     * Sap xep voi thuat toan Bubble Sort
     */
    public void sort2() {
        Boolean flag = true;
        int temp;
        while (flag) {
            flag = false;
            for (int i = 0; i < array.size() - 1; i++) {
                if (array.get(i) > array.get(i + 1)) {
                    flag = true;
                    temp = array.get(i);
                    array.set(i, array.get(i + 1));
                    array.set(i + 1, temp);
                }
            }
        }
    }

    public void reverse() {
        Collections.reverse(array);
    }

    public void reverse2() {
        Stack<Integer> s = new Stack();
        for (Integer x : array) {
            s.push(x);
        }
        int index = 0;
        while (!s.isEmpty()) {
            array.set(index, s.pop());
            index++;
        }
    }

    public void displayArray() {
        for (Integer x : array) {
            System.out.print(x + "    ");
        }
        System.out.println("\n");
    }

    public static void main(String[] args) throws IOException {
        Cau1 c1 = new Cau1();
        c1.getArray();
        System.out.println("Mang ban dau: ");
        c1.displayArray();
        c1.shuffle();
        System.out.println("Mang tron ngau nhien 1: ");
        c1.displayArray();
        c1.sort();
        System.out.println("Mang sap xep 1: ");
        c1.displayArray();
        c1.reverse();
        System.out.println("Lat nguoc mang 1: ");
        c1.displayArray();
        c1.shuffle2();
        System.out.println("Mang tron ngau nhien 2: ");
        c1.displayArray();
        c1.sort2();
        System.out.println("Mang sap xep 2: ");
        c1.displayArray();
        c1.reverse2();
        System.out.println("Mang lat nguoc 2: ");
        c1.displayArray();
    }
}
