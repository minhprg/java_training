import java.util.*;

public class Hangman
{
	static String key = "apple";
	
	static int[] open = new int[key.length()];
	static int numLetterSucess = 0;
	static int times = 0;
	static void print()
	{
		for(int i =0; i < key.length(); i++)
		{
			if(open[i] == 1)
				System.out.print(key.charAt(i));
			else
				System.out.print("-");
		}
	}
	public static void main(String[]args)
	{
		Scanner ip = new Scanner(System.in);
		char charIn;
		for(int i =0; i < key.length(); i++)
		{
			open[i] = 0;
		}
		print();
		do
		{
			System.out.println(" nhap 1 ki tu ");
			charIn = ip.nextLine().charAt(0);
			times++;
			for(int i = 0; i < key.length(); i++)
			{
				if(key.charAt(i) == charIn && open[i] == 0)
				{
					
					open[i] =1;
					numLetterSucess++;
				}
			}
			print();
			
		}
		while(numLetterSucess < key.length() && times < 10);	
		if(times >= 10)
		{
			System.out.println("\nyou lose!");
		}
		else 
		{
			System.out.println("\nyou win!");
		}
	}
}