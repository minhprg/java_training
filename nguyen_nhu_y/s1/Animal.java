public class Animal
{
	private String name;
	private int Age;
	
	public void setName( String name)
	{
		this.name = name;
	}
	public void setAge(int age)
	{
		if(age > 0 )
		this.Age = age;
	}
	public String displayName()
	{
		return name;
	}
	public int displayAge()
	{
		return Age;
	}
	public void speak()
	{
		System.out.println("Animal is speaking");
	}
}